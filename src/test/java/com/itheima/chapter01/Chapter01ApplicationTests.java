package com.itheima.chapter01;

import com.itheima.controller.HelloController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest //标记spring Boot 单元测试，并加载项目的ApplicationContext上下文环境
class Chapter01ApplicationTests {

    @Autowired
    private HelloController helloController;

    @Test
    void contextLoads() {
        String hello=helloController.hello();
        System.out.printf(hello);
    }

}
